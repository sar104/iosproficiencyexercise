//
//  Connectivity.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    
    class func isConnectedToInternet() -> Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
    
}
