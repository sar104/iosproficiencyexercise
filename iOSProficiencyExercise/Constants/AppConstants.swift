//
//  AppConstants.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

let NetworkStatusChanged = "NetworkStatusChanged"
let networkErrMSg = "No internet connection"
let alertTitle = "Alert"

let cellId = "cell"
let refreshButtonTitle = "Refresh"
