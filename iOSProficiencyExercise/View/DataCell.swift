//
//  DataCell.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

import SDWebImage

class DataCell: UITableViewCell {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        
        self.setLayoutConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        
        contentView.frame.inset(by: margins)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    private func setLayoutConstraints() {
        
        let marginGuide = contentView.layoutMarginsGuide
        
        self.contentView.addSubview(imgView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(descrLabel)
       
        
        imgView.setConstraint(top: nil, left: marginGuide.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, width: 70, height: 70)
        imgView.centerYAnchor.constraint(equalTo: marginGuide.centerYAnchor).isActive = true
        
        titleLabel.setConstraint(top: marginGuide.topAnchor, left: imgView.rightAnchor, bottom: nil, right: marginGuide.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: 0, height: 0)
        titleLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 252), for: .vertical)
        titleLabel.numberOfLines = 0
        
        descrLabel.setConstraint(top: titleLabel.bottomAnchor, left: imgView.rightAnchor, bottom: marginGuide.bottomAnchor, right: marginGuide.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 35, paddingRight: 10, width: 0, height: 0)
        descrLabel.numberOfLines = 0
        descrLabel.lineBreakMode = NSLineBreakMode.byWordWrapping

    }
    
    
    
    let imgView:UIImageView = {
        
        let img = UIImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        img.contentMode = .scaleAspectFit
        
        
        return img
    }()
    
    let titleLabel: UILabel = {
        
        let label = UILabel()
        
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.textColor = UIColor.black
        
        return label
    }()
    
    let descrLabel: UILabel = {
        
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.textColor = UIColor.black
        
        return label
    }()
    
    let lblview: UIView = {
        
        let view = UIView()
        
        return view
    }()
    
    var dataViewModel: DataViewModel? {
        
        didSet {
            guard let data = dataViewModel else {return}
            
            titleLabel.text = data.getTitle()
            descrLabel.text = data.getDescr()
            
            let url = URL(string: data.getImg())
            
            imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), options: .retryFailed) { (image, error, imgCacheType, url) in
                
            }
        }
    }
    
}
