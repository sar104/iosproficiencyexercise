//
//  WebServiceManager.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

final class WebServiceManager {
    
    private let webService: WebService!
    
    init(service: WebService) {
        self.webService = service
    }
    
    func getData(_ completion: @escaping ((Result<ResultData>) -> Void)){
        
        webService.fetchData(api: AppURL) { (result) in
            
            switch result {
                
            case .success(let data):
                do {
                    
                    let response = try JSONDecoder().decode(ResultData.self, from: data)
                    
                    completion(.success(response))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            
            
        }
    }
}
