//
//  DataViewController.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DataViewController: BaseViewController {
    
    var dataArr: [DataViewModel] = []
    let dataTableView: UITableView = UITableView()
    
    let serviceManager = WebServiceManager(service: WebService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // add observer for network change
        NotificationCenter.default.addObserver(self, selector: #selector(handleNetworkStatus), name: NSNotification.Name(rawValue: NetworkStatusChanged), object: nil)
        
        // setup UI
        setupUI()
        
        // make API call
        callAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleNetworkStatus() {
        
        self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
    }
    
    
    // MARK: API call method
    
    func callAPI() {
        
        self.showActivityIndicator()
        if Connectivity.isConnectedToInternet(){
            
            serviceManager.getData { (result) in
                
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                }
                
                switch result {
                    
                case .success(let data):
                    
                    if let resultData = data as? ResultData {
                        if let rows = resultData.rows {
                            for val in rows {
                                
                                self.dataArr.append(DataViewModel(data: val))
                            }
                        }
                        DispatchQueue.main.async {
                            self.setNavigation(title: resultData.title)
                        }
                    }
                    DispatchQueue.main.async {
                        
                        self.dataTableView.reloadData()
                    }
                case .failure(let error):
                    
                    self.alert(strTitle: alertTitle, strMsg: error.localizedDescription)
                }
            }
        } else {
            self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
        }
    }
    
    
    // MARK: UI setup
    
    func setupUI(){
        
        let refreshButton = UIBarButtonItem(title: refreshButtonTitle, style: .plain, target: self, action: Selector(("refreshData")))
        self.navigationItem.rightBarButtonItem = refreshButton
        
        view.addSubview(dataTableView)
        
        dataTableView.setConstraint(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
       
        dataTableView.rowHeight = UITableView.automaticDimension
        dataTableView.estimatedRowHeight = 100
        
        dataTableView.dataSource = self
        dataTableView.delegate = self
        dataTableView.separatorColor = .black
        dataTableView.separatorStyle = .singleLine
        dataTableView.separatorInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        
        dataTableView.register(DataCell.self, forCellReuseIdentifier: "cell")
    }
    
    func setNavigation(title: String){
        
        navigationItem.title = title
        
    }
    
    @objc func refreshData(){
        
        dataArr.removeAll()
        self.dataTableView.reloadData()
        callAPI()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: tableview datasource, delegate

extension DataViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DataCell
        cell.dataViewModel = dataArr[indexPath.row]
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.selectionStyle = .none
    }
}
