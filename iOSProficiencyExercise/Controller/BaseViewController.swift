//
//  BaseViewController.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var overlay : UIView?
    var loader = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showActivityIndicator() {
        
        hideActivityIndicator()
        if  let window = UIApplication.shared.keyWindow{
            overlay = UIView(frame:CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
            overlay!.alpha = 0
            overlay!.backgroundColor = UIColor.black
            window.addSubview(overlay!);
        }else{
            
            overlay = UIView(frame:self.view.frame)
            overlay!.alpha = 0
            overlay!.backgroundColor = UIColor.black
            self.view.addSubview(overlay!);
            
        }
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.7)
        overlay!.alpha = overlay!.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
        
        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        indicator.center = overlay!.center
        indicator.startAnimating()
        overlay!.addSubview(indicator)
        
        let delay = 100.0 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        loader = true
        DispatchQueue.main.asyncAfter(deadline: time) {
            if self.loader{
                self.hideActivityIndicator()
                print("HIDDEN FOR TIMEOUT")
            }
        }
    }
    
    func hideActivityIndicator() {
        if overlay != nil {
            loader = false
            overlay?.removeFromSuperview()
            overlay =  nil
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
