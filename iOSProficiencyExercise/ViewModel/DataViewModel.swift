//
//  DataViewModel.swift
//  iOSProficiencyExercise
//
//  Created by Apple on 4/22/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class DataViewModel {
    
    var responseData: ResponseData!
    
    init(data: ResponseData) {
        self.responseData = data
    }
    
    func getTitle() -> String {
        return responseData.title ?? ""
    }
    
    func getDescr() -> String {
        return responseData.description ?? ""
    }
    
    func getImg() -> String {
        return responseData.imageHref ?? ""
    }
}
